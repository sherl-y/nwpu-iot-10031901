from django.shortcuts import render
def indexView(request):
    """Main Page"""
    return render(request, 'index.html')