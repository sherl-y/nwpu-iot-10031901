from django.conf.urls import url
from django.contrib.auth.views import login
from . import views
urlpatterns = [
    url(r'^login/$', login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', views.logoutView, name='logout'),
    url(r'^register/$', views.registerView, name='register'),
]