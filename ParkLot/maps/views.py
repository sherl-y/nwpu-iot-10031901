from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse  # nani kore>?
from .models import Spot, Lot
import json
from .forms import SpotForm, orderForm
# release
from order.models import Order
# coding
# from ParkLot.order.models import Order
from django.contrib.auth.decorators import login_required
from .models import Spot

def gmapView(request):
    center = [108.910462, 34.245147]
    return render(request, 'gmap.html', {
        'Center': json.dumps(center)
    })

@login_required
def new_spot(request):
    if request.method != 'POST':
        form = SpotForm()
    else:
        # POST
        form = SpotForm(request.POST)
        if form.is_valid():
            form.personal = True
            form.save()  # data_save
            return HttpResponseRedirect(reverse('index:index'))  # set url by appointed URL
    context = {'form': form}
    return render(request, 'new_spot.html', context)

# @login_required
#def createOrder(request, id):
#     if request.method != 'POST':
#
#     # 注文に応じる
#     # いまのユーザーにオーダーをつくって
#     new_order = Order.objects.create()