from django import forms
from .models import *
# release
from order.models import Order
# Coding
# from ..order.models import Order
class SpotForm(forms.ModelForm):
    class Meta:
        model = Spot
        fields = ['latitude', 'longitude', 'personal']  # busy_invalid id_auto
        labels = {'latitude': '', 'longitude': ''}
        # longitude = forms.FloatField(labels='longitude')
        # latitude = forms.FloatField(labels='latitude')

class orderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['spot']
        labels = {'spot': ''}