from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$', views.gmapView, name='gmap'),
    url(r'^new_spot/$', views.new_spot, name='new_spot'),
    # url(r'^new_order/$', views.createOrder, name='new_order')
]