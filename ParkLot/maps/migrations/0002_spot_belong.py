# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2022-07-06 22:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='spot',
            name='belong',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='maps.Lot'),
            preserve_default=False,
        ),
    ]
