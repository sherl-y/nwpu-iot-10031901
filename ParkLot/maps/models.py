from django.db import models
from django.contrib.auth.models import User

class Lot(models.Model):
    id = models.AutoField(primary_key=True)
    # 0 for Indoor; 1 for Outdoor;
    type = models.BooleanField('IO')
    longitude = models.FloatField('Longitude')
    latitude = models.FloatField('Latitude')
    nums = models.IntegerField('spots')
    idle = models.IntegerField('idle')

class Spot(models.Model):
    """Spot"""
    # txt = models.CharField(max_length=50)
    # def __str__(self):
    #     """return str(Spot)"""
    #     return self.txt
    id = models.AutoField(primary_key=True)
    belong = models.ForeignKey(Lot)
    longitude = models.FloatField('Longitude')
    latitude = models.FloatField('Latitude')
    busy = models.BooleanField('used', default=False)
    key = models.CharField('key', max_length=50)
    personal = models.BooleanField('personal', default=False)
    # after user login
    owner = models.ForeignKey(User, default=False)
    # type private or public
    # private -- belong to someone