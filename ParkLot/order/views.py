from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from .models import *

@login_required
def orderView(request):
    # orders = Order.objects.get(user = )
    # context = {'orders': orders}
    # return render(request, 'order.html', context)
    return render(request, 'order.html')