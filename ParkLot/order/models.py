from django.db import models
from maps.models import Spot, Lot

class Order(models.Model):
    id = models.AutoField(primary_key=True)
    spot = models.ForeignKey(Spot)
    # user = models.ForeignKey(User)
    # active when True
    active = models.BooleanField(default=True)
    # auto_now_add constraint
    startTime = models.DateTimeField(auto_now_add=True)
    # auto_now time during alter
    stopTime = models.DateTimeField(auto_now=True)