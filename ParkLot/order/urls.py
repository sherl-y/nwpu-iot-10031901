from django.conf.urls import url
from . import views

urlpatterns = [
    # まだお客さんのIDは確認させないので、
    url(r'^$', views.orderView, name='my_order')
]