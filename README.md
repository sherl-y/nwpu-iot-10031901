# 仓库描述

本仓库为 2019 级西北工业大学物联网专业物联网综合实验课程的项目文件。

## 仓库目录结构

```tree
├─Document
│  ├─pics
│  └─功能设计
├─ParkLot
│  ├─index
│  │  ├─migrations           
│  │  └─templates      
│  ├─maps
│  │  ├─migrations        
│  │  └─templates
│  │  │  ├─css
│  │  │  ├─img
│  │  │  └─js
│  ├─order
│  │  ├─migrations
│  │  └─templates
│  ├─ParkLot
│  ├─TST
│  │  ├─css
│  │  ├─img
│  │  └─js
│  └─users
│      ├─migrations
│      └─templates
└─src

```

## 如何部署

基于 Django 的网页编程环境，安装方法：

```
pip install django
```

### 系统要求

1. 边缘服务器节点要求：略
2. 传感器节点要求：略略路
